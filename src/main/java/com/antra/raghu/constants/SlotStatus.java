package com.antra.raghu.constants;

public enum SlotStatus {
	
	PENDING, ACCEPTED, REJECTED, CANCELLED
	
}
